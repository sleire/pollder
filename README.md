# Modelling and Pricing Air Quality Index Derivatives

Stochastic Modelling of Ambient Air Quality and Pricing of Air Pollution Derivatives

**Reproduce environment and results in Docker**

Clone repository to local folder, for example /home/myuser/pollder and build container image from project root

`docker build --tag pollder .`

Run the container and mount local pollder folder to container pollder folder

`docker run -d  -e PASSWORD=pollder -p 5000:8787 --name pollder -v /home/myuser/pollder:/home/rstudio/pollder pollder`

Login Rstudio with username `rstudio` and password `pollder` at

`http://0.0.0.0:5000`

Shut down container with

`docker stop pollder`
